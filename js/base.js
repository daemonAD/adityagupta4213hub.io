$(".skills").typed({
    strings: ["Websites", "User Interfaces", "Landing Pages", "Web Applications"],
    typeSpeed: 50,
    backDelay: 700,
    loop: true,
});
$('#collaborate').click(function () {
    $('.collaborate-container').css('height', '100vh');
    $('.collaborate-container').css('width', '100vw');
});
$('.close').click(function () {
    $('.collaborate-container').css('height', '0');
    $('.collaborate-container').css('width', '0');
});


$('#contact-form').on('submit', function(e) {
  $('.close').click();
    $('#collaborate').html('Done');
    $('#collaborate').attr('id', 'done');
  });s

var year = new Date().getFullYear();
$('.year').html(year);

$('.nav-link').click(function () {

     $("html, body").animate({
         scrollTop: 0
     }, 600);

     return false;

 });